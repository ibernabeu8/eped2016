package listasT4;

import es.uned.lsi.eped.DataStructures.Iterator;
import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;

public class ListaRecursivos {

	// =============================================  RESURSIVOS ====================================================
	//1. [*] Encontrar el máximo (mínimo) de una lista.
	public static int[] buscarMaximoMinimo(ListIF<Integer> listaNumeros) {
		return auxBuscarMaxMin(listaNumeros.get(1),listaNumeros.get(1), listaNumeros);
	}
	
	private static int[] auxBuscarMaxMin(int maximoActual, int minimoActual, ListIF<Integer> listaNumeros) {
		
		int[] max_min = new int[2];
		//caso base
		if(listaNumeros.isEmpty()) {
			max_min[0] = maximoActual;
			max_min[1] = minimoActual;
			return max_min;
		//caso recursivo
		}else{
			int numeroActual = listaNumeros.get(1);
			if(maximoActual < numeroActual) maximoActual = numeroActual;
			if(minimoActual > numeroActual) minimoActual = numeroActual;
		}
		//System.out.println("Maximo: " + maximoActual);
		//System.out.println("Maximo: " + minimoActual);
		listaNumeros.remove(1);
		return auxBuscarMaxMin(maximoActual, minimoActual, listaNumeros);
	}
	
	
	//2. [*] Substituir todas las apariciones de un ciert elemento en una lista por otro elemento.
	public static ListIF<Object> substituir(Object sustituto, Object objetivo, ListIF<Object> lista) {
		ListIF<Object> listaFinal = new List<Object>();
		return auxSubstituir(sustituto, objetivo, lista, listaFinal);
	}
	private static ListIF<Object> auxSubstituir
	(Object sustituto, Object objetivo, ListIF<Object> lista, ListIF<Object>listaFinal) {
		
		//caso base, lista vacia
		if(lista.isEmpty()) {
			return listaFinal;
		}
		else {
			Object actual = lista.get(1);
			if(actual.equals(objetivo)) {
				listaFinal.insert(sustituto, (listaFinal.size()+1));//lo añadimos al final
			}//si son iguales
			else {
				listaFinal.insert(actual, (listaFinal.size()+1));			
			}
			lista.remove(1);
			return auxSubstituir(sustituto, objetivo, lista, listaFinal);
		}
	}
	
	//3. [*] Determinar si una lista está ordenada.
	public static boolean estaOrdenadoNumeros(ListIF<Integer> lista) {
		//casos base
		if(lista.isEmpty()) return false;
		if(lista.size() == 1) return true;
		
		if(lista.size() > 2) {
			int ordenado = Integer.compare(lista.get(1), lista.get(2));
			
			if( ordenado < 0) {
				return false;
			}else{//recursivo
				lista.remove(1);
				return estaOrdenadoNumeros(lista);
			}
		}
		return lista.get(1) > lista.get(2);
	}

	/* 4.[*] Sumar todos los elementos de una lista tales
			que estén rodeados por dos elementos
			menores a ellos mismos.*/
	public static int sumaMenoresAdyacentes(ListIF<Integer> lista) {
		//caso
		if(lista.isEmpty()) return 0;
		if(lista.size() < 3) return 0;
		
		if(lista.get(2) > lista.get(1) && lista.get(2) > lista.get(3)) {
			int elemento = lista.get(2);
			lista.remove(1);
			return sumaMenoresAdyacentes(lista) + elemento;
		}
		
		lista.remove(1);
		return sumaMenoresAdyacentes(lista);
	}
	/* 5.[**] Determinar si existe en una lista un rellano
		(un cierto número de elementos contiguos) que
		son iguales. Discutir las diferencias:
		
		a. En una lista cualquiera.
		b. En una lista ordenada.
	*/
	public static boolean esRellano(ListIF<Integer> lista) {		
		//c.base
		if(lista.isEmpty() || lista.size() == 1) return false;	
		if(lista.get(1) == lista.get(2)) return true;
		//recursivo
		lista.remove(1);
		return esRellano(lista);
	}
	// 6. [*]Mezclar dos listas ordenadas (manteniendo el orden). MENOR A MAYOR
	public static ListIF<Integer> ordenarListas(ListIF<Integer> lista1, ListIF<Integer> lista2) {
		return auxOrdenarListas(lista1, lista2, null);
	}
	private static ListIF<Integer> auxOrdenarListas(ListIF<Integer> lista1, ListIF<Integer> lista2,
													ListIF<Integer> listaFinal) {
		
		//si no esta creada la instanciamos
		if(listaFinal == null) listaFinal = new List<Integer>();
		//casos bases
		if(lista1.isEmpty() && lista2.isEmpty()) 
			return listaFinal;
		else if(lista1.isEmpty()) {
			listaFinal.insert(lista2.get(1), listaFinal.size()+1);
			lista2.remove(1);
		}
		else if(lista2.isEmpty()) {
			listaFinal.insert(lista1.get(1), listaFinal.size()+1);
			lista1.remove(1);
		}else{	
			// casos recursivos
			int comparador = Integer.compare(lista1.get(1), lista2.get(1));
			
			// numero lista 1 mayor que el de lista 2
			if (comparador > 0) {
				listaFinal.insert(lista2.get(1), listaFinal.size() + 1);
				lista2.remove(1);
			}
			// numero lista 2 mayor que el de lista 1
			else if (comparador < 0) {
				listaFinal.insert(lista1.get(1), listaFinal.size() + 1);
				lista1.remove(1);
			}
			// son iguales
			else {
				listaFinal.insert(lista2.get(1), listaFinal.size() + 1);
				listaFinal.insert(lista1.get(1), listaFinal.size() + 1);
				lista1.remove(1);
				lista2.remove(2);
			}
		}
		return auxOrdenarListas(lista1, lista2, listaFinal);
	}
	
	//7. [**] Determinar si una lista es sublista de otra.  (Es sublista B de la lista A)
	public static boolean esSublista(ListIF<Integer> listaA , ListIF<Integer> listaB) {
		//Caso base
		if(listaA.isEmpty()) return false;
		
		else if(auxSublista(listaA, listaB) == false && !(listaA.isEmpty())) {
			listaA.remove(1);
			return esSublista(listaA, listaB);
		}else{
			return true;
		}	
	}
	//este metodo recorre la lista y las vacia siempre que haya coincidencia
	private static boolean auxSublista(ListIF<Integer> listaA , ListIF<Integer> listaB) {
		
		if(listaA.get(1) == listaB.get(1)) {
			listaA.remove(1);
			listaB.remove(1);
			return auxSublista(listaA, listaB);
		}else{
			return false;
		}
	}
	
	//8.[**/✤] Invertir una lista.TIPO EXAMEN
	private static ListIF<Object> invertirLista(ListIF<Object> lista) {
		return auxInvertirLista(lista, new List<Object>());
	}
	private static ListIF<Object> auxInvertirLista(ListIF<Object> listaOriginal, ListIF<Object> listaFinal) {
		//caso base
		if(listaOriginal.isEmpty()) return listaFinal;
		else {
			Object ultimoElemento = listaOriginal.get(listaOriginal.size());
			listaFinal.insert(ultimoElemento, listaFinal.size()+1);
			listaOriginal.remove(listaOriginal.size());
			return auxInvertirLista(listaOriginal, listaFinal);
		}
	}
	
	//9.[**/✤] Determinar si una lista es palíndroma o capicúa (las posiciones a igual distancia de su centro
	//		contienen el mismo elemento) . Por simplificar, voy hacer solo si es capicua , que es referente a numeros
	private static boolean capicua(ListIF<Integer> lista) {
		//caso base
		if(lista.size()%2 == 0) return false; //si el numero de elementos no es par no puede ser capicua
		//
		return false;
	}//SIN TERMINAR
	
	//10.[**/✤] Determinar si una lista tiene un elemento que es suma de todos sus anteriores.
	private static boolean elementoSuma(ListIF<Integer> lista) {
		return auxElementoSuma(lista, 0);
	}
	private static boolean auxElementoSuma(ListIF<Integer> lista, int sumaActual) {
		if(lista.isEmpty()) return false;
		else if(lista.get(1) == sumaActual) return true;
		else {
			sumaActual += lista.get(1);
			lista.remove(1);
			return auxElementoSuma(lista, sumaActual);
		}
	}
	//11. [**] Determinar si una lista es una cordillera (una lista con tramos crecientes y decrecientes
	//encadenados, en la que no existen rellanos (tramos iguales); por ejemplo [1,2,3,2,4,6,3,1,5,7,2]).
	
	//12. [***] Mezclar dos listas desordenadas para obtener otra ordenada.
	
	public static void main(String[] args) {
		
	
		 List<Integer> L = new List<Integer>();
		 L.insert(15, 1);
		  L.insert(2, 2);
		  L.insert(3, 3);
		  L.insert(4, 4);
		  L.insert(5, 5);
		  L.insert(6, 6);
		  L.set(5, 10);
		 
		  int[] maximo_minimo = buscarMaximoMinimo(L);
		  int maximo = maximo_minimo[0];
		  int minimo = maximo_minimo[1];
		  System.out.println("Maximo: " + maximo + " Minimo: " + minimo);
		  
		  
		  //ejercicio 2
		  String a = "patronA";
		  String b = "patronB";
		  String c = "patronC";
		  
		  ListIF<Object> lista = new List<Object>();
		  lista.insert(a, 1);
		  lista.insert(b, 2);
		  lista.insert(c, 3);
		  lista.insert(a, 4);
		  lista.insert(b, 5);
		  lista.insert(c, 6);
		  lista.insert(a, 7);
		  lista.insert(a, 8);
		  lista.insert(a, 9);
		  
		  
		  List<Object> listafinal = (List<Object>) substituir("PatronASustituido", "patronA", lista);
		  
		  for(int i = 1; i < listafinal.size(); i++) {
			  System.out.println(listafinal.get(i).toString());
		  }
		  
		  //EJERCICIO 3
			 L = new List<Integer>();
			 L.insert(15, 1);
			 L.insert(2, 2);
			 L.insert(3, 3);
			 L.insert(4, 4);
			 L.insert(5, 5);
			 L.insert(6, 6);
			 L.set(5, 10);
			 
			 List<Integer> L2 = new List<Integer>();
			 L2.insert(15, 1);
			 L2.insert(6, 2);
			 L2.insert(5, 3);
			 L2.insert(4, 4);
			 L2.insert(2, 5);
			 L2.insert(1, 6);
			 
		  //System.out.println(estaOrdenado(L));
		  System.out.println(estaOrdenadoNumeros(L2));
		  
		 //EJERCICIO 4
		  ListIF<Integer> L3 = new List<Integer>();
		  L3.insert(5, 1);
		  L3.insert(4, 2);
		  L3.insert(6, 3);
		  L3.insert(7, 4);
		  L3.insert(2, 5);
		  L3.insert(3, 6);
		  L3.insert(1, 7);
		  // 5 , 4 , 6 , 7 , 2 , 3 , 1 
		  System.out.println(sumaMenoresAdyacentes(L3));
		  
		  //EJERCICIO 5
		  ListIF<Integer> L5 = new List<Integer>();
		  L5.insert(5, 1);
		  L5.insert(4, 2);
		  L5.insert(6, 3);
		  L5.insert(4, 4);
		  L5.insert(2, 5);
		  L5.insert(3, 6);
		  L5.insert(3, 7);  
		  
		  System.out.println(esRellano(L5));

		  //EJERCICIO 6
		  ListIF<Integer> L61 = new List<Integer>();
		  L61.insert(5, 1);
		  L61.insert(10, 2);
		  L61.insert(14, 3);

		  
		  ListIF<Integer> L62 = new List<Integer>();
		  
		  L62.insert(1, 1);
		  L62.insert(11, 2);
		  L62.insert(20, 3);
		  
		  Iterator<Integer> it = (Iterator<Integer>) ordenarListas(L61,L62).iterator();

		  while(it.hasNext()) {
			  System.out.print(it.getNext() + " ");
		  }
		  
		  //EJERCICIO 7, NO FUNCIONA!!!!!!!!!!!!!!!!!!!!!!!!!
		  ListIF<Integer> L71 = new List<Integer>();//[5,1,11,20,14]
		  L71.insert(5, 1);
		  L71.insert(1, 2);
		  L71.insert(11, 3);
		  L71.insert(20, 4);
		  L71.insert(14, 5);
		  
		  ListIF<Integer> L72 = new List<Integer>();//[1,11,20]
		  
		  L72.insert(1, 1);
		  L72.insert(11, 2);
		  L72.insert(20, 3);
		  System.out.println("\n");
		  System.out.println(esSublista(L71, L72) + "\n");
		  
		  //EJERCICIO 8
		  ListIF<Object> L81 = new List<Object>();//[5,1,12,20,14]
		  L81.insert(5, 1);
		  L81.insert(1, 2);
		  L81.insert(12, 3);
		  L81.insert(20, 4);
		  L81.insert(14, 5);
		  
		 List<Object> listainvertida =  (List<Object>) invertirLista(L81);
		 //EL FOR HAY QUE EMPEZARLO EN 1 , Y EL LIMITE SIZE +1 , PORQUE LA LISTA DE LOS TADS EMPIEZA EN 1 NO EN 0
		 for(int i = 1; i < listainvertida.size()+1; i++) {
			 System.out.print(listainvertida.get(i) + " ");
		 }
		 System.out.println("\n");
		 //EJERCICIO 9
		 
		 //EJERCICIO 10
		  ListIF<Integer> L10 = new List<Integer>();//[5,1,6,20,14], debería ser true
		  L10.insert(5, 1);
		  L10.insert(2, 2);
		  L10.insert(10, 3);
		  L10.insert(19, 4);
		  L10.insert(14, 5);
		  System.out.println(elementoSuma(L10));
	}

}
