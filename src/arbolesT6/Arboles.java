package arbolesT6;

import es.uned.lsi.eped.DataStructures.BTree;
import es.uned.lsi.eped.DataStructures.IteratorIF;
import es.uned.lsi.eped.DataStructures.ListIF;
import es.uned.lsi.eped.DataStructures.Tree;
import es.uned.lsi.eped.DataStructures.TreeIF;

public class Arboles {
	
	
	public static Integer maximo(TreeIF<Integer> arbol) {
		if (arbol.isEmpty())
			return 0;
		return maximoAux(arbol.getChildren(), arbol.getRoot());
	}

	private static Integer maximoAux(ListIF<TreeIF<Integer>> arbol, int maximo) {

		for (int i = 1; i < arbol.size() + 1; i++) {
			if (arbol.get(i).getRoot() > maximo) {
				maximo = arbol.get(i).getRoot();
			}
			maximoAux(arbol.get(i).getChildren(), maximo);
		}
		return maximo;

	}

	/*
	 * [*] Encontrar el máximo (mínimo) de un árbol general (binario) en los siguientes casos:
	 * 		a. El árbol no está ordenado (sin orden implícito).
	 * 		b. El árbol está ordenado por claves (valores).
	 */
/*	
	public int searchMax(TreeIF<Integer> arbolEnteros) {
		
		if(arbolEnteros instanceof Tree)
			return searchMaxNario((Tree<Integer>)arbolEnteros,0);
		
		if(arbolEnteros instanceof BTree)
			return searchMaxBinario((BTree<Integer>)arbolEnteros);
		else
			return 0;
	}
	
	private int searchMaxNario(Tree<Integer> arbolEnteros, int maxActual) {
		int raiz = arbolEnteros.getRoot();
		ListIF<TreeIF<Integer>> subArboles = arbolEnteros.getChildren();
		
		if(raiz > maxActual) 
			maxActual = raiz;
		
		if(subArboles.isEmpty() && arbolEnteros.isLeaf())
			return maxActual;
		else
			
			
	}
	
	private int searchMaxBinario(BTree<Integer> arbolEnteros) {
		
	}
*/

	
	//FIN EXAMEN
	public static void main(String[]args) {
		TreeIF<Integer> arbol = new Tree<Integer>();

		TreeIF<Integer> hijode = new Tree<Integer>();
		TreeIF<Integer> hijodede = new Tree<Integer>();
		TreeIF<Integer> hijoiz = new Tree<Integer>();

		arbol.setRoot(0);
		hijode.setRoot(5);
		hijodede.setRoot(8);
		hijoiz.setRoot(16);

		hijode.addChild(1, hijodede);
		arbol.addChild(1, hijode);
		arbol.addChild(2, hijoiz);
		
		IteratorIF<Integer> it = arbol.iterator(Tree.POSTORDER);
		
		while(it.hasNext()) {
			System.out.print(it.getNext());
		}
		
		
		//System.out.println(maximo(arbol));
		
	}
}
