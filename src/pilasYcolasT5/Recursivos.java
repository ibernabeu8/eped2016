package pilasYcolasT5;

import es.uned.lsi.eped.DataStructures.IteratorIF;
import es.uned.lsi.eped.DataStructures.Queue;
import es.uned.lsi.eped.DataStructures.Stack;
import es.uned.lsi.eped.DataStructures.StackIF;

public class Recursivos {

	/*
	 * 1.[*] Encontrar el máximo (mínimo) de una pila (cola):
	 *  a. Destruyendo la estructura original.
	 *  b. [*+] Sin destruirla (sin iteradores).
	 */
	//Destructiva, maximo con pila
	public static int maxElementStackD(StackIF<Integer> pila) {
		return maxElementStackD(pila,pila.getTop());
	}
	
	private static int maxElementStackD(StackIF<Integer> pila, int maxActual) {
		//Entero donde almacenaremos el sigueinte elemento
		int siguiente = maxActual;
		//Cogemos el elemento mas arriba de la pila (si no esta vacia)
		if(!pila.isEmpty())
			siguiente = pila.getTop();
		else 		
			return siguiente > maxActual ? siguiente : maxActual;
		//Comparamos el maximo actual con el siguiente numero
		if(siguiente > maxActual)
			maxActual = siguiente;
		//Llamada recursiva
		pila.pop();//eliminamos la cabeza de la pila
		return maxElementStackD(pila, maxActual);
	}	
	//Sin destruir, minimo con cola
	public static int minElementQueueD(Queue<Integer> cola) {
		return minElementQueueD(cola, cola.getFirst());
	}
	private static int minElementQueueD(Queue<Integer> cola, int minActual) {
		//siguiente elemento en la cola
		int siguiente = minActual;
		//comprobamos que no este vacia 
		if(!cola.isEmpty())
			siguiente = cola.getFirst();
		else
			return siguiente < minActual ? siguiente : minActual;
		if(siguiente < minActual)
			minActual = siguiente;
		//LLamada recursiva
		cola.dequeue();
		return minElementQueueD(cola, minActual);
	}
	
	/*
	 * 2. [*] Substituir todas las apariciones de un cierto elemento en una pila (cola) por otro elemento.
	 */
	public static Stack<Integer> replaceElement(Stack<Integer> pila, int elemento, int elementoPila) {
		//primero vaciamos la pila en orden inverso
		Stack<Integer> pilaAux = new Stack<Integer>();
		IteratorIF<Integer> iteratorStack = pila.iterator();
		
		while(iteratorStack.hasNext()) {
			pilaAux.push(iteratorStack.getNext());
		}
		return replaceElement(pilaAux,new Stack<Integer>(), elemento, elementoPila);
	}
	//lo que hace es recibir una pila y devolverla invertida sustituyendo las apareiciendo de elemento pila por elemento
	private static Stack<Integer> replaceElement(Stack<Integer> pila,Stack<Integer> nuevaPila, int elemento, int elementoPila) {
		//caso base
		if(pila.isEmpty())
			return nuevaPila;
		//recursivo
		int actual = pila.getTop();
		System.out.println("el numnero actual es: " + actual);
		if(actual == elementoPila)
			nuevaPila.push(elemento);
		else
			nuevaPila.push(actual);
		//vaciamos la cabeza de la pila
		pila.pop();
		return replaceElement(pila, nuevaPila, elemento, elementoPila);
			
	}
	public static void main(String[]args) {
		
		//EJERCICIO 1 
		Stack<Integer> pila = new Stack<Integer>();
		
		pila.push(2);
		pila.push(10);
		pila.push(1);
		pila.push(7);
		pila.push(0);
		
		System.out.println("El elemento maximo es: " + maxElementStackD(pila));
		//EJERCICIO 2
		Queue<Integer> cola = new Queue<Integer>();
		
		cola.enqueue(32);
		cola.enqueue(3);
		cola.enqueue(30);
		cola.enqueue(29);
		cola.enqueue(18);
		
		System.out.println("El elemento minimo es: " + minElementQueueD(cola));
		
		//EJERCICIO 3
		pila.push(10);
		pila.push(0);
		pila.push(23);
		pila.push(40);
		pila.push(10);
		pila.push(7);
		pila.push(10);
		
		pila = replaceElement(pila, 5, 10);//cambia los 10s por 5s
		
		IteratorIF<Integer> iteratorPila = pila.iterator();
		
		while(iteratorPila.hasNext()) {
			System.out.print(iteratorPila.getNext() + " ");
		}
	}
}
