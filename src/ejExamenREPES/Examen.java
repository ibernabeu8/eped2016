package ejExamenREPES;

import es.uned.lsi.eped.DataStructures.IteratorIF;
import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;
import es.uned.lsi.eped.DataStructures.Queue;
import es.uned.lsi.eped.DataStructures.QueueIF;
import es.uned.lsi.eped.DataStructures.Stack;
import es.uned.lsi.eped.DataStructures.StackIF;
import es.uned.lsi.eped.DataStructures.Tree;
import es.uned.lsi.eped.DataStructures.TreeIF;

public class Examen {
	//===================================== LISTAS ========================================
	/*
	 * [**] Invertir una lista.
	 */
	public static ListIF<Integer> invertirLista(ListIF<Integer> lista) {
		
		return invertirListaR(lista, new List<Integer>());
	}
	
	private static ListIF<Integer> invertirListaR(ListIF<Integer> listaInicial, ListIF<Integer> listaFinal) {
		//CASO BASE
		if(listaInicial.isEmpty()) return listaFinal;
		//Recursivo
		int elemento = listaInicial.get(listaInicial.size());
		listaFinal.insert(elemento, listaFinal.size()+1);
		listaInicial.remove(listaInicial.size());
		return invertirListaR(listaInicial,listaFinal);
	}
	
	/*
	 * [**] Determinar si una lista es palíndroma o capicúa 
	 * (las posiciones a igual distancia de su centro contienen el mismo elemento)
	 */
	
	public static boolean esPalindromo(ListIF<Integer> lista) {
		if(lista.get(1) != lista.get(lista.size())) return false;
		else {
			lista.remove(1);//primero
			lista.remove(lista.size());//ultimo
			return lista.size() > 1 ? esPalindromo(lista) : true;
		}
	}
	 
	/*
	 * [**] Determinar si una lista tiene un elemento que es suma de todos sus anteriores.
	 */
	
	public static boolean sumaAnteriores(ListIF<Integer> lista) {
		return sumaAnteriores(lista, 0);
	}
	private static boolean sumaAnteriores(ListIF<Integer> lista, int acumulador) {
		//CASO BASE
		if(lista.isEmpty()) return false;
		//
		if(lista.get(1) == acumulador) return true;
		else {
			acumulador += lista.get(1);
			lista.remove(1);
			return sumaAnteriores(lista, acumulador);
		}
	}
	//========================================FIN ================================================
	
	//===================================== PILAS Y COLAS ========================================
	 /*
	  * [**] Evaluar una expresión postfija (Notación Polaca Inversa. Por ejemplo, [3,2,+,4,5,*,-] sería en 
	  * notación infija y parentizada (3+2)-(4*5)) (elementos 
	  * separados en postfijo para evitar ambigüedad con los valores) . ????ENUNCIADO??
	  */
	
	/* [**] Invertir una pila (cola). */
	public static StackIF<Integer> invertirPila(StackIF<Integer> pila) {
		return invetirPila(pila, new Stack<Integer>());
	}
	private static StackIF<Integer> invetirPila(StackIF<Integer> pilaOriginal, StackIF<Integer> pilaFinal) {
		//Caso base
		if(pilaOriginal.isEmpty()) return pilaFinal;
		//
		pilaFinal.push(pilaOriginal.getTop());
		pilaOriginal.pop();
		return invetirPila(pilaOriginal, pilaFinal);
	}
	
	public static QueueIF<Integer> invertirCola(QueueIF<Integer> cola) {
		return invertirCola(cola, new Stack<Integer>());
	}
	private static QueueIF<Integer> invertirCola(QueueIF<Integer> colaOriginal,Stack<Integer> pilaAux) {
		//caso base
		if(colaOriginal.isEmpty()) return pasarPila(new Queue<Integer>(),pilaAux);
		//
		pilaAux.push(colaOriginal.getFirst());
		colaOriginal.dequeue();
		return invertirCola(colaOriginal, pilaAux);
		
	}
	private static QueueIF<Integer> pasarPila(QueueIF<Integer> colaFinal, Stack<Integer> pilaAux) {
		if(pilaAux.isEmpty()) return colaFinal;
		else {
			colaFinal.enqueue(pilaAux.getTop());
			pilaAux.pop();
			return pasarPila(colaFinal, pilaAux);
		}
	}
	
	/*
	 * [**] Determinar si una pila (o una cola) es palíndroma o capicúa
	 */
/*	public boolean StackIF<Integer> isPalindroma(StackIF<Integer> pila) {
		
	}*/
	//========================================FIN ================================================
	
	//===================================== ARBOLES========================================
	/*
	 * Determinar si un árbol general es una fuente, es decir, tal que su recorrido en orden
	 * es una secuencia palíndroma o capicúa .
	 */
	/*
	public boolean esCapicua(TreeIF<Integer> arbol) {
		return esPalindromo(arbolInOrder(arbol, new List<Integer>()));
	}
	
	private ListIF<Integer> arbolInOrder(TreeIF<Integer> arbolActual, ListIF<Integer> listaFinal) {
		List<TreeIF<Integer>> hijos = (List<TreeIF<Integer>>) arbolActual.getChildren();
		
		if(!(arbolActual.getChildren().isEmpty())) {
			for(int i = 1; i < hijos.size(); i++) {
				
				//comprobar si es hoja
				if(i == 1 && hijos.get(i).isLeaf()) {
					listaFinal.set(hijos.get(i).getRoot(), listaFinal.size()+1);
					listaFinal.set(arbolActual.getRoot(), listaFinal.size()+1);
					
				}else if(hijos.get(i).isLeaf()){
					listaFinal.set(hijos.get(i).getRoot(),listaFinal.size()+1);
				}else{
					arbolInOrder(hijos.get(i), listaFinal);
				}
			}
		}else{
			
		}
	}*/
	
	//========================================FIN ================================================
	public static void main(String[]args) {
		
		//LISTAS EJ1
		List<Integer> lista = new List<Integer>();
		lista.insert(4, lista.size()+1);
		lista.insert(8, lista.size()+1);
		lista.insert(2, lista.size()+1);
		lista.insert(1, lista.size()+1);
		
		List<Integer> listaFinal = (List<Integer>) invertirLista(lista);
		IteratorIF<Integer> it= listaFinal.iterator();
		while(it.hasNext()) {
			System.out.println(it.getNext().toString());
		}
		//LISTA EJ2
		List<Integer> listaPalidroma = new List<Integer>();
		lista.insert(1, lista.size()+1);
		lista.insert(2, lista.size()+1);
		lista.insert(1, lista.size()+1);
		lista.insert(1, lista.size()+1);
		lista.insert(1, lista.size()+1);
		
		System.out.println(esPalindromo(lista));
		
		//LISTA EJ3
		List<Integer> listaSuma = new List<Integer>();
		listaSuma.insert(1, listaSuma.size()+1);
		listaSuma.insert(4, listaSuma.size()+1);
		listaSuma.insert(0, listaSuma.size()+1);
		listaSuma.insert(6, listaSuma.size()+1);
		
		System.out.println(sumaAnteriores(listaSuma));
		
		//PILAS Y COLAS EJ1
		Stack<Integer> pila = new Stack<Integer>();
		pila.push(4);
		pila.push(5);
		pila.push(6);
		pila.push(7);
		pila.push(8);
		
		System.out.println();
		System.out.println("INVERTIR PILA");
		System.out.println();
		
		IteratorIF<Integer> itPila0 = pila.iterator();
		while(itPila0.hasNext())  {
			System.out.print(itPila0.getNext());
		}
		Stack<Integer> pilaInvertida = (Stack<Integer>) invertirPila(pila);
		System.out.println();
		IteratorIF<Integer> itPila = pilaInvertida.iterator();
		
		while(itPila.hasNext())  {
			System.out.print(itPila.getNext());
		}
		
		System.out.println();
		System.out.println();
		System.out.println("INVERTIR COLA 12345");
		System.out.println();
		
		QueueIF<Integer> cola = new Queue<Integer>();
		cola.enqueue(1);
		cola.enqueue(2);
		cola.enqueue(3);
		cola.enqueue(4);
		cola.enqueue(5);
		QueueIF<Integer> colaFinal = new Queue<Integer>();
		
		colaFinal = invertirCola(cola);
		IteratorIF<Integer> itColaInicial = cola.iterator();
		
		while(itColaInicial.hasNext()) {
			System.out.print(itColaInicial.getNext());
		}
		
		IteratorIF<Integer> itCola = colaFinal.iterator();
		while(itCola.hasNext()) {
			System.out.print(itCola.getNext());
		}
	}
}

