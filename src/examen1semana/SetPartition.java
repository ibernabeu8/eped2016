package examen1semana;

import es.uned.lsi.eped.DataStructures.IteratorIF;
import es.uned.lsi.eped.DataStructures.ListIF;
import es.uned.lsi.eped.DataStructures.SetIF;

public class SetPartition<T> implements SetPartitionIF<T> {
	
	private ListIF<T> listaParticiones;
	
	//constructor
	public SetPartition(SetIF<T> s) {
		IteratorIF<T> it = s.iterator();
		
		while(it.hasNext()) {
			listaParticiones.insert(it.getNext(), listaParticiones.size());
		}
	}
	@Override
	public void addGroup(T e) {
		listaParticiones.insert(e, listaParticiones.size());
	}

	@Override
	public void removeGroup(T e) {
		int i = 1;//posicion
		IteratorIF<T> it = listaParticiones.iterator();
		while(it.hasNext()) {
			if(it.getNext().equals(e))
				listaParticiones.remove(i);
			i++;
		}
	}

	@Override
	public SetIF<T> getGroup(T e) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void merge(T e1, T e2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean checkPartition(SetIF<T> S) {
		// TODO Auto-generated method stub
		return false;
	}

}
