package examen1semana;

import es.uned.lsi.eped.DataStructures.SetIF;

public interface SetPartitionIF<T> {
	/*Añade el grupo unitario (e) a la particion */
	public void addGroup(T e);
	/* Elimina de la partición el grupo al que pertenece el elemento e */
	public void removeGroup(T e);
	/* Devuelve el grupo al que pertenece el elemento e */
	public SetIF<T> getGroup(T e);
	/* Mezcla los grupos a los que pertenecen los elementos e1 y e2 */
	public void merge(T e1, T e2);
	/* Decide si la particion es valida con respecto al conjunto S dado por parametro.
	 * Esto es, comprueba que todos los elementos de S pertenecen a algun grupo de la particion y que todos los
	 * grupos de la particion distintos entre si son disjuntos, i.e. no comparten ningun elemento entre si
	 */
	public boolean checkPartition(SetIF<T> S);
}
